<?php

/**
 *  引入 composer 自动加载文件
 */

use Core\BeanFactory;

include_once __DIR__."/vendor/autoload.php";
include_once __DIR__."/app/config/define.php";
/*
use Swoole\Http\Request;
use Swoole\Http\Response;


// 初始化服务器 0.0.0.0 绑定所有ip  端口 9501
$http = new Swoole\Http\Server("0.0.0.0",9501);

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {

});

// 回调方式执行
$http->on("request",function(Request $request,Response $response)use ($dispatcher){
    $myrequest = \App\core\Request::init($request);
    $routeInfo = $dispatcher->dispatch($myrequest->getMethod(), $myrequest->getUri());

    switch ($routeInfo[0]) {
        case FastRoute\Dispatcher::NOT_FOUND:
            $response->status(404);
            //不输出时 火狐一片空白
            $response->end("not found");
            break;
        case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
//            $allowedMethods = $routeInfo[1];
            $response->status(405);
            $response->end();
            break;
        case FastRoute\Dispatcher::FOUND:
            $handler = $routeInfo[1];
            $response->end($handler());
            break;
    }
});

//启动服务
$http->start();*/

BeanFactory::init();
